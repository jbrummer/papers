<!--
Thanks for using Papers!

If you want to ask for a feature request, please search through the current issues, there is a big chance that the request has been asked already.

When reporting bugs about Papers, it is most helpful to include the document you were viewing. If you can provide a link to this document please include that link so the Papers developers can test against that document as well.

See also https://wiki.gnome.org/Apps/Evince/PopplerBugs#How_to_check_if_a_bug_belongs_to_Poppler_or_Evince to learn how to check if a bug belongs to Poppler or Papers.

An ideal bug report looks like below:
-->
### Summary

REPLACE-ME: Explain in short what is the problem, e.g: crashed while opening PDF document

### Description

REPLACE-ME: more detailed description of the problem. If the problem happens on a specific document, please add a link or attach the document. If Papers is crashing, a backtrace is always useful. A good example would be: "I clicked on a link from Epiphany which opened Papers for viewing a PDF document. Papers crashed before actually rendering anything at all. Attached you can find the document and the backtrace after the crash"
