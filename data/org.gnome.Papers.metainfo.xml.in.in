<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright © 2014 Christian Persch

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope conf it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<component type="desktop-application">
  <id>@APP_ID@</id>
  <launchable type="desktop-id">@APP_ID@.desktop</launchable>
  <metadata_license>GPL-2.0+ or GFDL-1.3-only</metadata_license>
  <project_license>GPL-2.0-or-later</project_license>
  <name>Papers</name>
  <summary>Read documents</summary>
  <description>
    <p>A document viewer for the GNOME desktop. You can view, search or annotate documents in many different formats.</p>
    <p>Papers supports documents in: PDF, PS, EPS, XPS, DjVu, TIFF, and Comic Books archives (CBR, CBT, CBZ, CB7).</p>
  </description>
  <provides>
    <id>papers.desktop</id>
  </provides>
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <supports>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </supports>
  <screenshots>
    <screenshot type="default">
    <image>https://gitlab.gnome.org/GNOME/Incubator/papers/raw/main/data/screenshots/papers-1.png</image>
    <caption>A clear, simple UI</caption>
    </screenshot>
    <screenshot>
    <image>https://gitlab.gnome.org/GNOME/Incubator/papers/raw/main/data/screenshots/papers-2.png</image>
    <caption>Advanced highlighting and annotation</caption>
    </screenshot>
  </screenshots>
  <url type="homepage">https://apps.gnome.org/Papers</url>
  <url type="bugtracker">https://gitlab.gnome.org/GNOME/Incubator/papers/-/issues/new</url>
  <url type="translate">https://wiki.gnome.org/TranslationProject</url>
  <url type="donation">https://www.gnome.org/donate/</url>
  <url type="vcs-browser">https://gitlab.gnome.org/GNOME/Incubator/papers/</url>
  <project_group>GNOME</project_group>
  <developer id="org.gnome">
    <name>The GNOME Project</name>
  </developer>
  <update_contact>https://discourse.gnome.org/tag/papers</update_contact>
  <translation type="gettext">papers</translation>
  <content_rating type="oars-1.1" />
  <releases>
    <release version="46.1" type="stable" date="2024-05-23">
      <description>
        <p>This is the first Papers release! Papers is a fork of Evince ported to GTK4 and with a refreshed UI and a new group of maintainers. There has been several some other changes compared to Evince:</p>
        <ul>
          <li>DVI files are no longer supported.</li>
          <li>synctex support has been temporarily dropped (see !90).</li>
          <li>Drop the button and the toolbar to create annotations. Note annotations can be created from an option in the menu, and highlight annotations can be created upon selection of the desired text.</li>
          <li>Papers no longer tracks every opened document. Clicking on an already opened document from Nautilus will open a new window with a copy of the document, instead of bringing the already opened one into the background. This is a necessary change to bring a tabbed view at some point in the future.</li>
          <li>Drop the ability for users reload documents. Documents will be automatically reloaded if a change in the file is detected.</li>
          <li>Drop the auto-scrolling feature. This was done due to the possibly low amount of users and as to improve maintainability. Could be considered in the future if there are enough interested users.</li>
          <li>Text selection with tripple button now works if clicked multiple times.</li>
          <li>Drag and drop text and images has been removed as a necessary cleanup of the internals. It might be reconsidered in the future where more modernizations of the application have finished.</li>
        </ul>
        <p>Known problems:</p>
        <ul>
          <li>Some people are experiencing problems with printing through flatpak.</li>
          <li>There might be some performance issues on big documents or in documents with images, when having the sidebar opened showing the thumbnails. They are currently under investigation.</li>
        </ul>
      </description>
    </release>
    <release version="46.0" type="stable" date="2024-05-23">
      <description>
        <p>This is the first Papers release! Papers is a fork of Evince ported to GTK4 and with a refreshed UI and a new group of maintainers. There has been several some other changes compared to Evince:</p>
        <ul>
          <li>DVI files are no longer supported.</li>
          <li>synctex support has been temporarily dropped (see !90).</li>
          <li>Drop the button and the toolbar to create annotations. Note annotations can be created from an option in the menu, and highlight annotations can be created upon selection of the desired text.</li>
          <li>Papers no longer tracks every opened document. Clicking on an already opened document from Nautilus will open a new window with a copy of the document, instead of bringing the already opened one into the background. This is a necessary change to bring a tabbed view at some point in the future.</li>
          <li>Drop the ability for users reload documents. Documents will be automatically reloaded if a change in the file is detected.</li>
          <li>Drop the auto-scrolling feature. This was done due to the possibly low amount of users and as to improve maintainability. Could be considered in the future if there are enough interested users.</li>
          <li>Text selection with tripple button now works if clicked multiple times.</li>
          <li>Drag and drop text and images has been removed as a necessary cleanup of the internals. It might be reconsidered in the future where more modernizations of the application have finished.</li>
        </ul>
        <p>Known problems:</p>
        <ul>
          <li>Some people are experiencing problems with printing through flatpak.</li>
          <li>There might be some performance issues on big documents or in documents with images, when having the sidebar opened showing the thumbnails. They are currently under investigation.</li>
        </ul>
      </description>
    </release>
  </releases>
</component>
